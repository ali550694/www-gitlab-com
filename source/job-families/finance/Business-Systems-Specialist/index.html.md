---
layout: job_family_page
title: "Business System Specialist"
---

## Business Systems Specialist - Product Transactions

## Responsibilities

* Identify and document new processes and methods of reducing incoming requests which help both internal and external users
* Triage / troubleshoot issues and find workarounds where possible
* Handle all incoming transactions and licensing requests from users and internal GitLab team members
* Submit and contribute to bug reports and feature requests/improvements
* Work closely with the Fulfillment group to escalate and prioritize issues
* Work with the surrounding GitLab teams to help solve problems at scale
* Contribute to the appropriate FAQ’s, documentation, and handbook entries and work with GitLab’s technical writers to ensure consistency.
* Maintain good ticket performance and satisfaction
* Meet or exceed agreed SLA times consistently for requests

## Requirements

* 3-5 years experience in an administrative role, preferably senior
* Excellent written and spoken communication
* The ability to absorb abstract and complex problems and then communicate these problems clearly and concisely to others
* Patience, kindness and empathy
* Ability to work competently with a variety of different groups across GitLab
* A proactive self-starter who can think strategically whilst thriving in a reactive environment
* Highly organised and methodical with true attention to detail
* A passion for helping others and problem solving

## Nice to have

* Some experience with Rails applications
* Basic understanding of software development and programming
* Experience working in a support-desk style environment
* Experience with Zuora, Salesforce and similar CRM/billing and subscription tools
* A love of open source 
* Experience with SaaS products 
* Experience using GitLab/Git


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our VP of Field Operations or Director of Sales Operations
* Candidates will then be invited to schedule a third interview with our Marketing Operations Lead
* Candidates will be then be invited to schedule a call with our FP&A Lead
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

