---
layout: job_family_page
title: "Engineering Management - Infrastructure"
---

## Engineering Manager, Reliability

The Engineering Manager, Reliability directly manages the engineering team relentlessly focused on GitLab.com's
performant reliability, a team composed of [DBRE](/job-families/engineering/database-reliability-engineer/)s and
[SRE](/job-families/engineering/site-reliability-engineer/)s. They see the team as their product. They may work on
small features or bugs to keep their technical skills sharp and stay familiar with the code, but they emphasize
hiring a world-class team and putting them in the best position to succeed. They own the delivery of product
commitments and they are always looking to improve the productivity of their team. They must also coordinate
across departments to accomplish collaborative goals.

### Responsibilities
 - Hire an incredible team that lives our [values](/handbook/values/)
 - Improve the happiness and productivity of the team
 - Work on small features and bugs (nothing critical path)
 - Own Incident and Change Management, RCA, and Error Budget Management
 - Hold regular 1:1's with team members
 - Manage agile projects
 - Work across sub-departments within engineering
 - Improve the quality, security and performance of the product

### Requirements
 - 2-5 years managing software engineering teams
 - Demonstrated teamwork in a peak performance organization
 - Experience running a consumer scale platform
 - Product company experience
 - Enterprise software company experience
 - Computer science education or equivalent experience
 - Passionate about open source and developer tools
 - Exquisite communication skills
 - [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#management-group)

### Nice-to-have's
 - Online community participation
 - Remote work experience
 - Startup experience
 - Significant open source contributions

## Director of Engineering, Infrastructure

The Director of Engineering, Infrastructure manages multiple teams that work on GitLab.com and contribute to our on-premise product. They see their teams as their products. They are capable of managing multiple teams and projects at the same time. They are expert recruiters of both developers and managers. But they can also grow the existing talent on their teams. Directors are leaders that model the behaviors we want to see in our teams and hold others accountable when necessary. And they create the collaborative and productive environment in which developers and engineering managers do their work.

### Responsibilities

- Hire and manage multiple infrastructure teams that live our [values](/handbook/values/)
- Measure and improve the happiness and productivity of the team
- Define the agile development and continuous delivery process
- Drive quarterly OKRs
- Work across sub-departments within engineering
- Write public blog posts and speak at conferences
- Own the quality, security, availability, reliability and performance of the product

### Requirements

- 10 years managing multiple operations and software engineering teams
- Experience in a peak performance organization
- Kubernetes, Docker, Go, and Linux administration
- Product company experience
- Startup experience
- Experience with consume scale platforms
- Enterprise software company experience
- Computer science education or equivalent experience
- Passionate about open source and developer tools
- Exquisite communication skills

### Nice-to-have's

- Online community participation
- Remote work experience
- Significant open source contributions
