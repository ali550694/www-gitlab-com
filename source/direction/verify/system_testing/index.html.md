---
layout: markdown_page
title: "Category Vision - System Testing"
---

- TOC
{:toc}

## System Testing

Modern software is often delivered as a collection of (micro)services to multiple clouds, rather than a single monolith to your own data center. Validating complex interactions to ensure the reliability of the system as a whole is more important than ever.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3ASystem%20Testing)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Next up will be a proof of concept of using Docker Compose to run integration tests for a project or group of projects that consists of interdependent microservices as part of [gitlab-ce#22559](https://gitlab.com/gitlab-org/gitlab-ce/issues/22559).  From there, we will work with our internal customer to understand what, if any problems that solves with our current use of [`gitlab-qa`](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/what_tests_can_be_run.md#orchestrated-tests)

## Competitive Landscape

### API testing tools

API testing tools like [SoupUI Pro](https://www.soapui.org/professional/soapui-pro.html), [Apache JMeter](https://jmeter.apache.org/) and [Hoverfly](https://hoverfly.io/) allows users to automate API and microservices testing across multiple services.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category.

## Top Customer Issue(s)

Customers have requested we implement WebAssembly testing ([gitlab-ce#58212](https://gitlab.com/gitlab-org/gitlab-ce/issues/58212)) for system testing.

## Top Internal Customer Issue(s)

There are no top internal customer issues for this category currently.

## Top Vision Item(s)

The top vision issue for this category is [group-level review apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58600).