---
layout: markdown_page
title: "Category Vision - Usability Testing"
---

- TOC
{:toc}

## Usability Testing

GitLab has lots of capability for testing various aspects of software, but most are aimed around functional testing of code. Usability testing is a technique used in user-centered interaction design to evaluate a product by testing it on users. This can be seen as an irreplaceable usability practice, since it gives direct input on how real users use the system. 

Usability testing is slightly different than UAT in that the primary purpose of a usability test is to improve a design, rather than a gate to accept/reject a deployment. At GitLab, we're not big believers in manual gates to slow things down, but we are big believers in user-focused design.

One thing that we certainly want to communicate through our vision here is that we feel designers are important, and can contribute using GitLab. By adding features with them in mind, we can help them to contribute their best.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3AUsability%20testing)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

A good first step to introduce this category would be to offer review tools for review apps that support usability feedback review flows. [gitlab-ee#10761](https://gitlab.com/gitlab-org/gitlab-ee/issues/10761) is a step forward in that direction, which will inject a feedback toolbar (similar to the internal performance toolbar on gitlab.com) that allows you to submit feedback while browsing around a review app.

## Competitive Landscape

Usability testing is a feature offered primarily through integrations by competitive products. Taking advantage of Review Apps, we're able to offer a much nicer experience via [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) (manual review flow for review apps) where feedback can be requested on a review environment, bringing feedback much earlier into the development process and integrating that automatically with GitLab issues.

### Marker.io

[Marker.io](https://marker.io/) is a product that ships mostly as an extension to go into your browser and allows you to raise an issue with various issue providers (Trello, JIRA, GitHub and even GitLab).  It provides a very intuitive user interface for screenshots, annotations and other discussions.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category.

## Top Customer Issue(s)

There are no top customer issues for this category.

## Top Internal Customer Issue(s)

For usability testing, our internal team has asked for built-in support for visual regression testing ([gitlab-ce#57111](https://gitlab.com/gitlab-org/gitlab-ce/issues/57111)).

## Top Vision Item(s)

For the same reasons as the competitive landscape section, [gitlab-org&960](https://gitlab.com/groups/gitlab-org/-/epics/960) (manual review flow for review apps) offers a very cool feature which allows design teams to collect user feedback on review apps and other environments. This is a strong foundation for how we can expand on usability testing within GitLab, without building UAT gates into the product.

Beyond this, we can look at offering the same for mobile via a solution like https://headspin.io/products.