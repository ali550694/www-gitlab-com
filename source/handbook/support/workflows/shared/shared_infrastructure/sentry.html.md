---
layout: markdown_page
title: Sentry
category: Shared Infrastructure
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Sentry

This [deck](https://docs.google.com/presentation/d/1j1J4NhGQEYBY8la6lCK-N-bw749TbcTSFTD-ANHiels/edit#slide=id.p) (GitLab internal only) provides an introduction to using Sentry for tracking errors in GitLab.