---
layout: markdown_page
title: "RM.1.01 - Risk Assessment Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.1.01 - Risk Assessment

## Control Statement

GitLab management performs a risk assessment quarterly. Results from risk assessment activities are reviewed to prioritize mitigation of identified risks.

## Context

Risk assessments are important because they identify, prioritize, and help tracking the remediation of risks to GitLab. The purpose of this control is to set an appropriate cadence for risk assessments and ensure the mitigation efforts to address those risks are reasonably prioritized. The goal of a risk assessment is to convey what the risks and their priority are to the audience.

## Scope

TBD

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.01_risk_assessment.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.01_risk_assessment.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.01_risk_assessment.md).

## Framework Mapping

* SOC2 CC
  * CC3.1
  * CC3.2
  * CC3.3
  * CC3.4
  * CC5.1
  * CC5.2
* PCI
  * 12.2
