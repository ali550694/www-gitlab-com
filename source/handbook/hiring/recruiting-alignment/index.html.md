---
layout: markdown_page
title: "Recruiting Alignment"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Recruiter and Coordinator Alignment by Hiring Manager

| Sales                    | Recruiter       | Coordinator     |
|--------------------------|-----------------|-----------------|
| Michael McBride          | Kelly Murdock   | April Hoffbauer |
| Francis Aquino           | Kelly Murdock   | April Hoffbauer |
| Paul Almeida             | Kelly Murdock   | April Hoffbauer |
| Kristen Lawrence         | Kelly Murdock   | April Hoffbauer |
| Richard Pidgeon          | Nadia Vatalidis | Kike Adio       |

| Marketing                                      | Recruiter       | Coordinator     |
|------------------------------------------------|-----------------|-----------------|
| Erica Lindberg                                 | Jacie Zoerb     | Chantal Rollison |
| Ashish Kuthiala                                | Jacie Zoerb     | Chantal Rollison |
| LJ Banks                                       | Jacie Zoerb     | Chantal Rollison |
| Alex Turner                                    | Jacie Zoerb     | Chantal Rollison |
| Evan Whelchel                                  | Jacie Zoerb     | Chantal Rollison |
| Director of Marketing Operations (to be hired, LJ Banks as interim) | Jacie Zoerb     | Chantal Rollison |
| David Planella                                 | Seán Delea      | Betti Gal        |
| Melissa Smolensky                              | Jacie Zoerb     | Chantal Rollison |
| Elsje Smart                                    | Seán Delea      | Betti Gal        |

| Engineering               | Recruiter                                               | Coordinator |
|---------------------------|---------------------------------------------------------|-------------|
| Quality                   | Rupert Douglas                                          | Kike Adio        |
| UX                        | Rupert Douglas                                          | Kike Adio        |
| Support                   | Cyndi Walsh                                             | Emily Mowry      |
| Infrastructure            | Matt Allen                                              | Emily Mowry      |
| Security                  | Cyndi Walsh                                             | Emily Mowry      |
| Development - Dev         | Catarina Ferreira                                       | Kike Adio        |
| Development - Secure      | Liam McNally                                            | Kike Adio        |
| Development - Ops         | Eva Petreska                                            | Emily Mowry      |
| Development - Enablement  | Trust Ogor                                              | Kike Adio        |
| Development - Growth      | Trust Ogor                                              | Kike Adio        |
| Leadership                | Steve Pestorich                                         | Emily Mowry      |

| Product             | Recruiter                       | Coordinator |
|---------------------|---------------------------------|-------------|
| Product Management  | Matt Allen                      | Emily Mowry |
| Technical Writing   | Matt Allen                      | Emily Mowry |

| Other         | Recruiter                       | Coordinator               |
|---------------|---------------------------------|---------------------------|
| Paul Machle   | Jacie Zoerb/Stephanie Garza     | Chantal Rollison          |
| Carol Teskey  | Jacie Zoerb/Seán Delea          | Chantal Rollison/Betti Gal     |
| Brandon Jung  | Kelly Murdock                   | April Hoffbauer           |
| Meltano       | Matt Allen                      | Emily Mowry               |

## Sourcer Alignment by Division and Location

| Product                   | Region            | Sourcer                | Estimated % |
|---------------------------|-------------------|------------------------|-------------|
| Sales & Marketing         | Americas/Anywhere | Stephanie Garza        | 100%        |
| Sales & Marketing         | EMEA & APAC       | Anastasia Pshegodskaya | 25%         |
| Engineering Development   | Anywhere          | Zsuzsanna Kovacs       | 100%        |
| Engineering General       | Anywhere          | Anastasia Pshegodskaya | 50%         |
| Other/Director+ Roles     | Anywhere          | Anastasia Pshegodskaya | 25%         |
